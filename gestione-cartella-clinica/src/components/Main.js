import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header.js';
import TablePazienti from './TablePazienti.js';

class Main extends React.Component{

    render(){
        return  <div>
                    <Header title="Gestione Cartella Clinica Pazienti"/>
                    <TablePazienti/>
                </div>
    }
}

export default Main;

