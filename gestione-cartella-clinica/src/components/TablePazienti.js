import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import TableEsami from './tableEsami';


class TablePazienti extends React.Component{
    constructor(props){
        super();
        this.state = {
            pazienti:[],
            codicefiscaleToPass:""
        }
    }
    setPazienti=()=>{
        this.setState({
            pazienti:this.listaPazienti
        })
    }

    getCodiceFiscale=()=>{
        return this.state.codicefiscaleToPass
    }

    listaPazienti = [{
        codiceFiscale: "GLTMHL95S20G478G",
        nome: "Michele",
        cognome: "Giulietti",
        dataNascita : "20/11/1995"
    },{
        codiceFiscale : "XXXXXX20X47X234Y",
        nome : "Pinco",
        cognome : "Pallino",
        dataNascita : "25/12/1990"
    },{
        codiceFiscale : "XGXHDJ39H78H238J",
        nome: "Ciccio",
        cognome : "Pasticcio",
        dataNascita : "30/05/1920"
    }]

    
    showTable=()=>{
        return <div><div><table className="table">
                    <thead>
                        <tr>
                            <th scope="col">Codice Fiscale</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Cognome</th>
                            <th scope="col">Data di Nascita</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.pazienti.map((paziente,index)=>(
                            <tr key={index} scope="row">
                                <td>{paziente.codiceFiscale}</td>
                                <td>{paziente.nome}</td>
                                <td>{paziente.cognome}</td>
                                <td>{paziente.dataNascita}</td>
                                <td><button className="btn btn-secondary" onClick={
                                    ()=>{
                                        this.setState({
                                            codicefiscaleToPass : paziente.codiceFiscale
                                        })
                                    }
                                }>Cerca</button></td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <button className="btn btn-primary" onClick={(this.setPazienti)}>Inizio App</button>
                </div>
                    <TableEsami query={this.getCodiceFiscale()}/>       
                </div>
                
    }

    render(){
        return this.showTable()
    }
}

export default TablePazienti;