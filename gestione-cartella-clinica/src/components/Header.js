import React from 'react';
import ReactDOM from 'react-dom';

class Header extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            title : this.props.title
        }
    }

    getTitle=()=>{
        return this.state.title
    }

    render(){
        return  <div>
                    <h1>{this.getTitle()}</h1>
                </div>
    }
}

export default Header;