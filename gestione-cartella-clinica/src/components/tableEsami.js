import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';


class TableEsami extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            cartella : [{
                idCartella: "01",
                codiceFiscale: "GLTMHL95S20G478G",
                esami:[{
                    idEsame:"N01",
                    tipoEsame:"Risonanza Magnetica",
                    dottoreEsame : "Dott. Mario Rossi"
                },{
                    idEsame:"N02",
                    tipoEsame:"Colonscopia",
                    dottoreEsame : "Dott. Mario Blu"
                }]
                
            },{
                idCartella: "02",
                codiceFiscale : "XXXXXX20X47X234Y",
                esami:[{
                    idEsame:"N02",
                    tipoEsame:"Colonscopia",
                    dottoreEsame : "Dott. Mario Blu"
                }]
                
            },{
                idCartella: "03",
                codiceFiscale : "XGXHDJ39H78H238J",
                esami:[{
                    idEsame:"N03",
                    tipoEsame:"Gastroscopia",
                    dottoreEsame : "Dott. Mario Verde"
                }]
            }],
        }
    }
    

    showTable=()=>{
    
        return (<div><h1>Esami Paziente</h1>
                    <div><table className="table">
                    <thead>
                        <tr>
                            <th scope="col">IdEsame</th>
                            <th scope="col">Tipo di Esame</th>
                            <th scope="col">Nominativo Dottore</th>
                        </tr>
                    </thead>
                    <tbody>
                        {   
                            this.state.cartella
                            .filter((cartella)=>
                                cartella.codiceFiscale==this.props.query
                            )
                            .map((esame)=>(
                                    esame.esami.map((val,index)=>
                                        <tr key={index}>
                                            <td>{val.idEsame}</td>
                                            <td>{val.tipoEsame}</td>
                                            <td>{val.dottoreEsame}</td>
                                        </tr>
                                    
                                    )
                             
                        ))}
                    </tbody>
                </table>
                </div>
                </div>)
                
    }

    render(){
        return  this.showTable()
    }
}

export default TableEsami;