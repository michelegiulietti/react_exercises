import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import '../css/App.css';

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isTextVisible: true
    }
  }

  toggleText() {
    this.setState({
      isTextVisible: !this.state.isTextVisible
    });
  }

  render() {
    var textToShow = this.state.isTextVisible
      ? "Hi,there!"
      : "I'm not here";

    return <div className="text-center my_padding">
      <div>
        <p style={{
            color: "blue"
          }} id="text">Testo prova interazione</p>
      </div>
      <div>
        <p style={{
            color: "red"
          }}>
          <strong>{textToShow}</strong>
        </p>
      </div>
      <button className="btn btn-primary" id="cliccami" onClick={this.showText.bind(this)}>Cliccami!</button>
    </div>
  }

}

export default Table;
