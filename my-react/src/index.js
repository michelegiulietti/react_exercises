import React from 'react';
import ReactDOM from 'react-dom';
import Table from './component/Table.js';
const contacts = [
    {name:"Michele"},
    {name:"Nadi"},
    {name:"Louise"},
    {name:"Giulio"}
];
 const element = <ul>
     {contacts.map((contacts,index)=>(
         <li key={index}>{contacts.name}</li>
     ))}
 </ul>

ReactDOM.render(<Table /> ,document.getElementById('root'));