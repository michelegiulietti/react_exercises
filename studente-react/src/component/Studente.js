import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';


class Studente extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      studenti:[]}
  }
  

 calculateAge(age){
  let currentYear = new Date().getFullYear();
  return currentYear - age;
}
 numberInterests(hobby){
    return hobby.length 
}

 showHobby(hobby){
   let result;
    for(let i=0;i<hobby.length;i++){
      if(i===0){
        result=hobby[i];
      }else{
        result+=","+hobby[i];
      }
    }
    return result
}
  setStudenti=()=>{
    this.setState({
      studenti : this.studentList
    });
  }
  studentList = [{
  name: "Michele",
  surname: "Giulietti",
  matricola: "001",
  birthDate: {
    year: 1995,
    month: 11,
    day: 20
  },
  hobby: ["Sports","Cocking","Developing"]
},
{
name: "Louise",
surname: "Arras",
matricola: "002",
birthDate: {
  year: 1997,
  month: 5,
  day: 13
},
hobby: ["Cocking", "Swimming"]
},{
name: "Giulio",
surname: "Galli",
matricola: "004",
birthDate: {
  year: 1992,
  month: 11,
  day: 30
},
hobby: ["Sports","Developing","Videogames","Music"]
}];

 showAnagrafica(){
  return <div>
          <table className="table text-center">
          <thead>
          <tr>
          <th scope = "col"> Name </th>
          <th scope = "col" > Surname </th>
          <th scope = "col" > Matricola </th>
          <th scope = "col" > Age </th>
          <th scope = "col" > Number of interests </th>
          <th scope = "col" > Interests </th>
          </tr></thead>
          <tbody>
            {this.state.studenti.map((studente,index)=>(
            <tr key={index}>
              <td>{studente.name}</td>
              <td>{studente.surname}</td>
              <td>{studente.matricola}</td>
              <td>{this.calculateAge(studente.birthDate.year)}</td>
              <td>{this.numberInterests(studente.hobby)}</td>
              <td>{this.showHobby(studente.hobby)}</td>
            </tr>))}
          </tbody>
          </table>
          <button className="btn btn-primary" onClick={this.setStudenti}>Popola Costruttore</button>
        </div>
 }

render(){
  return this.showAnagrafica()
}
}
export default Studente;
