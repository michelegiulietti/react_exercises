import React from 'react';
import ReactDOM from 'react-dom';

class Header extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            title : "I miei film preferiti"
        }
    }
    getTitle=()=>this.state.title

    render(){
        return <div className="container">{this.getTitle()}</div>
    }
}

export default Header;

