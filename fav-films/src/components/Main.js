import React from 'react';
import ReactDOM from 'react-dom';
import Header from './Header.js';
import Card from './Card.js';

class Main extends React.Component{
    render(){
        return  <div>
                    <Header/>
                    <Card/>
                </div>
    }
}


export default Main