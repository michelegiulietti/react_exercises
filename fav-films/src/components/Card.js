import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';



class Card extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            films:[]
        }
    }

    getName=()=>{
        return this.state.name;
    }

    getGenre=()=>{
        return this.state.genre;
    }

    getYear=()=>{
        return this.state.year;
    }

    getDirector=()=>{
        return this.state.director;
    }

    getImg=()=>{
        return this.state.img;
    }
    
    filmList=[
        {
            title:"Seven",
            genre:"dramatic",
            year: 1995,
            director: "David Fincher",
            img:"../img/seven.jpg"
        },
        {
            title:"Forrest Gump",
            genre:"Comedy/dramatic",
            year:1994,
            director:"Robert Zemeckis",
            img:"../forrestGump.jpg"
        },
        {
            title:"Apocalypto",
            genre: "Thriller/drama",
            year:2006,
            director:"Mel Gibson",
            img: "../apocalypto.jpg"
        }
        ];
    
    
    setFilm=()=>{
        this.setState({
            films : this.filmList
        })
    }
    
showFilms=()=>{
     return <div className="container">
                    <div className="row">
                        {
                            this.state.films.map((film,index)=>(
                                <div key={index} className="col-sm">
                                    <ul className="list-unstyled">
                                        <li className="media">
                                            <img src={film.img} className="mr-3" alt={film.title+" image"}/>
                                            <div className="media-body">
                                            <h5 className="mt-0 mb-1">{film.title}</h5>
                                                <ul>
                                                    <li>{film.year}</li>
                                                    <li>{film.director}</li>
                                                    <li>{film.genre}</li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            ))
                        }
                        <button className="btn btn-primary" id="start" onClick={this.setFilm}>Avvia Applicazione</button>
                    </div>
                </div>
}

    render(){
        return  this.showFilms();
    }
}

export default Card;